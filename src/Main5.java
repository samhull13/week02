import java.util.Scanner;

/**
 * Created by s1095797 on 1/30/2017.
 */

public class Main5
{
    public static double convertFahrenheitToCelsius(double fahrenheitTemperature) //method
    {

        return (fahrenheitTemperature - 32.0) * 5.0 / 9.0;
    }

    public static double convertCelsiusToFahrenheit(double celsiusTemperature) //method
    {
        double fahrenheit = 0.0;

        fahrenheit = celsiusTemperature * 9.0 / 5.0 + 32.0;

        return fahrenheit;
    }

    public static int convertCelsiusToFahrenheit(int celsiusTemperature) //method
    {
        int fahrenheit = 0;

        fahrenheit = celsiusTemperature * 9 / 5  + 32;

        return fahrenheit;
    }

    public static void main(String[] args)
    {
        // declare and initialize all variables
        Scanner scannerIn = new Scanner(System.in);
        double  fahrenheit = 0.0;
        double celsius = 0.0;
        double kelvin = 0.0;
        int count = 0;
        double temperatureSum = 0.0;
        double temperatureAverage = 0.0;
        java.lang.String keepGoing;

        // get the input
    do {

        System.out.print("Enter a fahrenheit temperature: ");
        fahrenheit = scannerIn.nextDouble();

        // do the calculations
        celsius = convertFahrenheitToCelsius(fahrenheit);
        temperatureSum += celsius;
        count++;

        System.out.print("Keep Going (Y/N)? ");
        keepGoing = scannerIn.next();

    } while (keepGoing.toUpperCase().equals("Y")); //always compare stings with .equals


    //} while (keepGoing.equals("Y") || keepGoing.equals("y"));
        //while (keepGoing == "Y"); the == operator does not work for strings




        temperatureAverage  = temperatureSum / (double)count;

        // display results

        System.out.println("Average celsius temperature is: " + temperatureAverage );
    }

}

