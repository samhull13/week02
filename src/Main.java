import java.util.Scanner;

public class Main
{

    public static void main(String[] args)
    {
        // declare and initialize all variables
        Scanner scannerIn = new Scanner(System.in);
        int temperature;

        temperature = 42;

        double fahrenheit = 32.0;
        double fahrenheitDoubled = 0.0;

        // get input

        System.out.print("Enter a fahrenheit temperature: ");
        fahrenheit = scannerIn.nextDouble();

        // perform calculations

        //fahrenheitDoubled *= 2.0;
        fahrenheitDoubled = fahrenheit * 2.0;

        // display the output

        // do not use calculations in an output statement
        //System.out.println("Twice the fahrenheit value is: " + (fahrenheit * 2.0));

        System.out.println("Twice the fahrenheit value is: " + fahrenheitDoubled );
    }
}
